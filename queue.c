/* 
 * C 编程技能测试实验代码
 * 为课程预检验
 */

/*
 * 程序实现一个同时支持 FIFO 和 LIFO 的队列
 *
 * 使用单链表表示队列元素
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "harness.h"
#include "queue.h"

/*
  创建空队列
  如果没有分配空间则返回 NULL
*/
queue_t *q_new()
{
    queue_t *q =  malloc(sizeof(queue_t));
    /* 如果 malloc 返回 NULL 呢? */
    if(!q)
    { 
      return NULL;
    }
    
    q->head = q->tail=NULL;
    q->legnth=0;
    return q;
}

/*
  释放队列所用到的所有空间
  如果 q 为 NULL 则没有效果
*/
void q_free(queue_t *q)
{
    /* 如何释放链表元素和字符串 */
    /* 释放队列结构 */
    if(q==NULL)
    {
      return;
    }
    if(q->head==NULL)
    {
       free(q);
       return; 
    }
    else if(q->head!=NULL)
    {
      list_ele_t *freekey;
      while(q->head!=NULL)
        {
        freekey = q->head;
        q->head = q->head->next;

        free(freekey->value);
        free(freekey);
        }
    }

    free(q);


}

/*
  在队首插入一个元素
  成功则返回 true
  q 为 NULL 或者 无法分配空间则返回 false 
  参数 s 指向待存储的字符串
  函数必须显式地分配空间并拷贝字符串到空间里面
 */
bool q_insert_head(queue_t *q, char *s)
{
    list_ele_t *newh;
    /* 如果 q 为 NULL你该怎么办? */
    if(q==NULL)
    { 
      return false; 
    }
    newh = malloc(sizeof(list_ele_t));
    /* 别忘了为字符串分配空间并拷贝它 */
   if(newh==NULL)
   {
      return false;//判断malloc失败时
   }
     //newh->value = malloc(sizeof(s));
      newh->value = malloc(strlen(s)+1);
    if(newh->value==NULL)
    {
      if(newh!=NULL)
           //free(newh->value);
          free(newh);
      return false;
    }
    strcpy(newh->value , s);

    if(q->head==NULL) 
    {
      q->tail =newh;   
    }
    /* 如果其中一个malloc 调用返回 NULL 该怎么办? */
    newh->next = q->head;
    q->head = newh;
    q->legnth++;
    return true;
}


/*
  在队尾插入一个元素
  成功则返回 true
  q 为 NULL 或者 无法分配空间则返回 false 
  参数 s 指向待存储的字符串
  函数必须显式地分配空间并拷贝字符串到空间里面
 */
bool q_insert_tail(queue_t *q, char *s)
{
    /* 你需要为该函数编写完整的代码 */
    /* 记住: 函数时间复杂度为 O(1) */
    list_ele_t *newt;
    /* 如果 q 为 NULL你该怎么办? */
    if(q==NULL)
    { 
      return false; 
    }
    newt = malloc(sizeof(list_ele_t));
      if(newt==NULL)
   {
      return false;//判断malloc失败时
   }
    /* 别忘了为字符串分配空间并拷贝它 */
    //newt->value = malloc(sizeof(s));//使用sizeof分配空间总是显示trace3报错
    //错误代码 Corruption detected in block with address 0x562daffc8ff0 when attempting to free it
    //设置断点查看块0x562daffc8ff0查不到，感觉应该是分配空间的问题，百度使用另一种方法分配空间
    //
    //修改为strlen(s)+1后trace03完成
    newt->value = malloc(strlen(s)+1);
    if(newt->value==NULL)
    {
      if(newt!=NULL)
           //free(newh->value);
          free(newt);
      return false;
    }
    strcpy(newt->value , s);

    if (q->head == NULL)
    {
        q->head = q->tail = newt;
    }
   else //队列不为空
    {
        //list_ele_t *temp = q->tail->next;
        q->tail->next= newt;
        q->tail = newt;
        newt->next =NULL;
        q->legnth++;
        //free(temp);   //销毁节点
    }
    return true;
}

/*
  移除队首元素Attempt to remove element from head of queue.
  成功则返回 true
  队列为 NULL 或者为 空 则返回 false 
  如果 sp 非空 并移除了一个元素，则将移除的字符串拷贝给 *sp
  (至多 bufsize-1 个字符, 加上1个 null 结束符（'\0',0）)
  元素和字符串使用的空间需要释放
*/
bool q_remove_head(queue_t *q, char *sp, size_t bufsize)
{
    
   if(q==NULL)
    { 
      return false; 
    }

    if (q->head == NULL)
    {
        //空链表
        return false;
    }
     if (q->head == q->tail)
    {
        //单个元素
        q->tail=NULL;
    }
    list_ele_t *delete = q->head;
    //q->head = delete->next;

    //char *temp = q->head->value;

    q->head = q->head->next;


    if(strlen(delete->value) >= bufsize-1)//参考描述
    {
      //delete->value[bufsize-2]='\0';
      delete->value[bufsize-1]='\0';
    }

    if(sp)
    {
    strcpy(sp, delete->value);
    }
    free(delete->value);
    free(delete);
    
    q->legnth--;
    

    return true;
}

/*
  返回队列的元素数量
  如果队列为 NULL 或者为 空 则返回 0
 */
int q_size(queue_t *q)
{
    /* 你需要为这个函数编写代码 */
    /* 记住: 函数时间复杂度为 O(1) */
    if (q==NULL)
    {
       return 0;
    }
    else 
    {
      return q->legnth;
    }
    

   
}

/*
  反转队列元素
  如果 q 为 NULL 则没有效果
  该函数不能直接或者间接调用其他函数为队列元素分配任何额外的空间
  (例如, 不能调用 q_insert_head, q_insert_tail, 或者 q_remove_head).
  应该是通过重排现有元素实现
 */
void q_reverse(queue_t *q)
{
    /* 你需要为这个函数编写代码 */
  //   if(q==NULL)
  //   { 
  //   }
  //  if(q!=NULL){
  //     list_ele_t *temp;
  //     list_ele_t *savetail;
  //     savetail=q->tail;
  //     temp = q->head;
  //     if(temp!=NULL&&temp->next!=NULL)//在trace06时候报错，增加条件后52分
  //     {
  //       do{    
  //         while(temp->next!=q->tail)
  //         {
  //           temp = temp->next;
  //         }
  //         q->tail->next = temp;
  //         q->tail = temp;
  //         q->tail->next = NULL;
  //         temp = q->head;
  //       }
  //       while(temp->next!=q->tail);
        
  //       q->tail->next = q->head;
  //       q->head->next = NULL;
  //       q->head = savetail;
  //       q->tail = temp;

  //       temp = NULL;  
  //  }

  //   }
  if(!q || !q->head)
      return;
    if(q->legnth == 1)
      return;

    list_ele_t *start, *end, *next_start;

    start = q->head->next;
    end = q->head;
    q->head->next = NULL;

    while(true)
    {
      next_start = start->next;
      start->next = end;
      end = start;
      start = next_start;

      if(start == NULL)
      {
        q->tail = q->head;
        q->head = end;
        return;
      }   
    }
}

